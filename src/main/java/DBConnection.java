/**
 * Created by ankit.manoj on 25/7/2017.
 */

import java.sql.*;

public class DBConnection
{
    static String dbURL = "jdbc:mysql://localhost:3306/GradlePractice?useSSL=false";
    private static String username = "root";
    private static String password = "A1n9k9i5t";

    private Connection dbConnection;

    public void getDBConnection() throws ClassNotFoundException, SQLException
    {
        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection(dbURL, username, password);
    }
    public int executeQuery(String query) throws ClassNotFoundException, SQLException
    {
        return dbConnection.createStatement().executeUpdate(query);
    }
}
